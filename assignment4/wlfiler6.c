#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	"wl.h"

/*
 *	wordlist table module version 4
 *
 *	functions are:
 *			init_table()	- set up this module
 *			in_table(str)	- sees if word is in table
 *			insert(str,val) - insert value into table
 *			lookup(str)	- retrieve value
 *			update(str,val) - update value in table
 *			word_delete(str)- delete the link containing str in its word
 *			firstword()	- return first word in table
 *			nextword()	- return next word in table
 *			get_head(str)	- finds the head of the appropriate list
 *			create_link(str, val) - creates a LINK object containing str and val, pointing to NULL
 */

/*
 *	data structure for this unit:  an array of linked lists of structs
 */

#define 	NUM_LETTERS 26

struct link {
		char	    *word;		/* the string	*/
		int	    value;		/* the count	*/
		struct link *next;		/* the next one	*/
	};
typedef struct link LINK;

LINK *get_head(char first);
void show_table();
LINK *create_link(char *str, int val);
LINK table[NUM_LETTERS];	/* creates array of (probably 26) link links */

/* global */
LINK *current_link;

/*
 *	init_table
 */

void
init_table()
{
	int i;
	for (i = 0; i < NUM_LETTERS; i++)
	{
		table[i].next = NULL;
		table[i].word = NULL;
		table[i].value = 0;
	}
}

/*
 *	int in_table(char str[])
 */

int in_table( char str[] )
{
	LINK *head = get_head(str[0]);
	LINK *linkp = head->next;

	while( linkp != NULL )
	{
		if ( strcmp( linkp->word, str ) == 0 )
		{
			return YES;
		}
		else
		{
			linkp = linkp->next;
		}
	}
	return NO;
}

/*
 *	int insert(str, val)
 *		purpose: add an entry to the table
 *		returns: NO if no more memory, YES if OK
 *		 method: add new node to head of list.  It's easy
 */

int
insert( char str[], int val )
{
	LINK *newlink = create_link(str, val);	/* create new link */
	LINK		*curr;			/* current link in loop */
	LINK		*prev;			/* previous link */

	/* insert the new link */
	curr = get_head(str[0]);			/* find head of the right list */
	prev = curr;
		/* find the link which newlink should be inserted after (but go beyond it in doing so)*/
	//while (curr->next != NULL && strcmp(curr->word, newlink->word) < 0)
	while(1)
	{
		if (curr->next == NULL)
		{
			if (curr->word == NULL)	/* head node */
				break;		/* prev should equal curr. We will insert after prev (head node) */
			if (strcmp(curr->word, newlink->word) < 0) /* earlier in alphabet, so insert after it */
				prev = curr;			/* we insert after prev, but have hit a wall at NULL */
			break;
		}
		if (curr->word != NULL && curr->next->word != NULL) /* prevent errors by checking */
		{
			if (strcmp(curr->word, newlink->word) > 0)
				break;
		}
		prev = curr;
		curr = curr->next;
	}
	newlink->next = prev->next;		/* point the new link to next one in line */
	prev->next = newlink;			/* point to the new link, connecting it into the list */

	return YES;
}



/*
 *	int lookup( str )
 */
int
lookup( char str[] )
{
	struct link *linkp;
	LINK *head = get_head(str[0]);
	for( linkp = head->next ; linkp ; linkp = linkp->next )
		if ( strcmp( linkp->word, str) == 0 )   /* if found	 */
			return linkp->value;		/* ret value     */
	return 0;					/* not found     */
}

/*
 * 	int update( str, val )
 */

int
update( char str[], int val )
{
	LINK *linkp;
	LINK *head = get_head(str[0]);
	for( linkp = head->next ; linkp ; linkp = linkp->next )
		if ( strcmp( linkp->word, str) == 0 ){  /* if found	*/
			linkp->value = val;		/* update	*/
			return YES;			/* and go	*/
		}
	return 0;					/* not found     */
}

/*
 * 	int delete( str )
 */

void
word_delete( char str[] )
{
	LINK *linkp;
	LINK *prev = get_head(str[0]);
	for( linkp = prev->next ; linkp ; prev = linkp, linkp = linkp->next )
		if ( strcmp( linkp->word, str) == 0 )
		{  /* if found	*/
			/* delete	*/
			prev->next = linkp->next;
			free(linkp->word);
			free(linkp);
			return;
		}
	return;					/* not found     */
}

/*
 *	char *firstword()
 */

char *
firstword()
{
	current_link = get_head('a');
	return nextword();
}

/*
 *	char *nextword()
 */

char *
nextword()
{
	static int x = 0;
	/* if this link points to the next word */
	if (current_link->next != NULL)
	{
		current_link = current_link->next;
		return current_link->word;
	}
	/* this link points to null, but we are not at the end of the array */
	if (x < 25)
	{
		x++;
		current_link = get_head('a' + x); /* this head should point to the next word, or come back here */
		return nextword();
	}
	/* this link points to null, and we are at the end of the array */
	return NULL;
}
LINK *
get_head(char first)
{
	LINK *head = &table[first - 'a']; 	//this may be a bug.?
	return head;
}

void 
show_table()
{
	int	     row;
	struct link *lp;

	for(row=0; row<26; row++)
	{
		printf("%3d [%c] [ | ]->", row, 'a'+row);
		for(lp=table[row].next;  lp ; lp = lp->next )
			printf("[%s|%d]->", lp->word, lp->value);
		printf("NULL\n");
	}
}

/**
 * create_link(char *str, int val)
 *
 * returns LINK pointer on success
 * returns NO on failure
 */

LINK *
create_link(char *str, int val)
{
	char *newstr;
	newstr = malloc(1 + strlen(str));	/* get memory for str	*/
	if (newstr == NULL)			/* or die		*/
		return NO;
	strcpy(newstr, str);			/* copy to mem		*/
	LINK *newlink = malloc(sizeof(LINK));	/* get mem for link	*/
	if (newlink == NULL)			/* or die		*/
		return NO;
	newlink->word = newstr;			/* put str in struct	*/
	newlink->value = val;			/* put val in struct	*/
	newlink->next = NULL;
	return newlink;
}
