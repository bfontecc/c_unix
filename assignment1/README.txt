PROBLEM 0: 
counter.cgi does not handle race conditions effectively, since Unix
can do multitasking and will access the hits file for 2 users, loading
the same integer for each under race conditions. It may then overwrite
hits with the second of the 2 "racing" users, causing the hits to be 
lower than the server has actually served up.

PROBLEM 1:
Used a finite state machine design. Works fine with tabs or semis

PROBLEM 2:
Had to add a length parameter along with the string. Works OK.

PROBLEM 3:
This is also state-based. It can be thrown a bit by 'orphan' double quotes
such as in comments.

PROBLEM 4:
This does not accept negative numbers.

PROBLEM 5:
http://www.people.fas.harvard.edu/~bfontecc/station_search.html

PROBLEM 6
Judging by the map at mbta.com, there are branches, such as the "wildcat branch" which can account for this phenomenon.

I used bootstrap.css from Twitter Bootstrap for my home page.
This file is too large to submit and has been deleted from submission
