#include <stdio.h>

/*
 * badtime.c
 * purpose: find incorrect time fields in train sched
 * input: text with fields delimited by semicolons and/or tabs
 * output: only the lines with malformed times 
 * errors: no error conditions 
 * usage: badtime < input > output 
 *
 */

#define LINELEN 10000

int main()
{
	char line[LINELEN];
	int line_num = 0;

	//loop through input line by line, until end of file
	while (fgets(line, LINELEN, stdin) != NULL)
	{
		//use scanf to try to get the values separated by colon
		int a = -1;
		int b = -1;
		sscanf(line, "%*[^';'];%*[^';'];%*[^';'];TI=%d:%d", &a, &b);
		//check if those values are invalid
		if ( (!(a < 24 && a > -1)) || (!(b < 60 && b > -1)) )
			printf("\n%d\n", line_num);
		line_num++;
	} //end while loop

}


