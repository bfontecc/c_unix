#include <stdio.h>

/*
 * extract_strings.c
 *
 * purpose: to display all the strings in a C program
 * input: C program as text
 * output: strings in the program as text, separated by carriage return
 * errors: no error conditions, but will give code instead of string output
 * 	if double quotes (\") appear by themselves.
 * usage: extract_strings < input > output
 */

//define states
#define NIN_STRING	0	//we're NOT in a string
#define IN_STRING 	1	//we're in a string

int flip(int);

int main()
{
	int state = NIN_STRING;
	int c = '\0';		//current character
	int lc;			//last character
	while (1)
	{
		lc = c;		//save last character (it could be an escape)
		c = getchar();	//read character
		if (c == EOF)
			break;	//end at EOF
		else if (c == '"')	//found a quotation mark
		{
			if (lc != '\\' && lc != '\'') //if not escaped or quoted
			{
				if (state == IN_STRING)	//this is a string's end
					putchar('\n');	//new line
				state = flip(state);	//change of state
			}
		}
		else
		{
			if (state == IN_STRING)		//print strings
				putchar(c);
		}	
	}
}

int flip(int in)
/*
 * purpose: help change the program's state
 *  simply outputs !in, assuming it is 1 or 0 input
 *  default output is 1
 */
{
	int out = (in == 1) ? 0: 1;
	return out; 
}
