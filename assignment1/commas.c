#include	<stdio.h>
#include 	<math.h>
#include	<stdlib.h>
#include	<string.h>

/*
 * commas.c
 *
 * purpose: separates groups of three digits with a comma
 * input: integer
 * output: string showing number with commas
 * errors: no error conditions
 * usage: commas < input > output
 */

#define STRSIZE 32

int get_a_number();
int is_all_digits(char[], int);

int main()
{
	//get a number from input
	int n = get_a_number();
	//convert the number to a string
	char str[32];
	sprintf(str, "%d", n);
	//find the string length
	int len = strlen(str);
	//find the first comma's offset (eg offset 2 for 10,000)
	int offset = (len % 3);
	//find the number of digit groups
	int num_commas = (len / 3);
	//if length is a multiple of 3, remove 'dangling' comma
	if (offset < 1)
		num_commas--;
	//output the number, with commas
	int pos = 0;
	while (pos < len)
	{
		//if not the beginning or end, place a comma at every third pos
		if (((pos - offset) % 3 == 0) && (num_commas > 0) && (pos > 0))
		{
			putchar(',');
			num_commas--;
		}
		//output the digit
		putchar(str[pos]);
		//keep going
		pos++;
	}
	putchar('\n'); 
}

int get_a_number()
/*
 * purpose: demand a positive integer from the user
 * returns: a positive integer
 * copied from hello6.c
 */
{
	int	inputval = -1;
	char	inputstr[STRSIZE];
	int	is_all_digits(char [], int);

	int len = strlen(inputstr);

	printf("Enter a number: \n");	/* prompt	*/
	fgets(inputstr, STRSIZE, stdin );	/* input	*/
	//find the integer value of the input
	//check if it was really a positive number
	if (is_all_digits(inputstr, len) == 1)
		inputval = atoi(inputstr);
	//if not, the loop will use -1 here
	//if the input is either not a digit, or has negative value
	while( inputval < 0 || is_all_digits(inputstr, len) == 0 )
	{
			printf("This is not a positive number: %s", inputstr );
	}
	return inputval;	/* send value back to caller */
}

int is_all_digits( char str[STRSIZE], int len )
/*
 * purpose: examine a string and see if all the chars are digits
 * returns: 1 if all chars before the newline are digits, 0 otherwise
 * bug?:    what if a string with no chars appears?
 */
{
	int	pos;			/* index into string	*/

	for (pos = 0; pos < len; pos++)
	{
		if ( ! isdigit((int)str[pos]) )	/* if not a dig	*/
		return 0;			/* get out now! */

	}
	return 1;			/* no problems		*/
}

