#include <stdio.h>

/*
 * rmtags.c
 * purpose: filter text, removing tags that end in '='
 * 	e.g. 'TR=070' should become '070'
 * input: text with fields delimited by semicolons and/or tabs
 * output: text without tags
 * errors: no error conditions, but may remove text other than tags if 
 * 	'=' present in text or no tags present
 * usage: rmtags < input > output
 *
 */

#define IN_TAG 		1
#define BET_TAGS 	2

int main()
{
	int c;
	int mode = IN_TAG;
	while ( (c=getchar()) != EOF ) //while not at file end
	{
		//look for semis, tabs, and newlines as state changing delimiters
		//we are in a tag at the beginning of a line or field
		if (c == ';' || c == '\t' || c == '\n')
		{
			putchar(c);	//do output them
			mode = IN_TAG;	//change state
		}
		if (mode == BET_TAGS)	//when we're between tags
		{
			putchar(c);	//just output what we find
		}
		if (c == '=')		//equals changes to between tag state
		{
			mode = BET_TAGS;
		}

	}	
}
