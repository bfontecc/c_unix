#include <stdio.h>

/*
 *  add1.c
 *  purpose: increment a counter
 *  input: an integer as text
 *  output: the next higher integer
 *  errors: if the input is not an integer, an arbitrary value will print
 *  usage: add1 < input >
 *
 */

int main()
{
	//use scanf to read input from stdin
	int i;
	scanf("%d", &i);
		
	//use printf to output the new count 
	printf("%d", ++i);
}
