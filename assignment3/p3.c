#include <stdio.h>

main()
{
	int m[5] = {4, 3, 2, 1, 0};
	int *p;
	p = m + 2;
	
	printf("m is %u, &m is %u, &m[0] is %u, m[0] is %u\n", m, &m, &m[0], m[0]);
	printf("p is %u, &p is %u, &p[0] is %u, p[0] is %u\n", p, &p, &p[0], p[0]);
}

