#include <stdio.h>
#include <string.h>

/**
 * p11.c
 *
 * This program is written as the solution to problem 11 of assignment 3
 *
 * purpose: reverse strings using two methods, array indexing and pointer arithmetic
 * 	test both methods
 * usage: p11
 * arguments: none
 *
 */

char *strrev(char *s);
char *strrev2(char *s);


/* main()
 *
 * test both functions, strrev, and strrev2
 */
int main()
{
	//test strrev (which uses indexing)
	char str_even1[7] = "Hello!";
	char str_odd1[6] = "12345";
	printf("str_even1: %s\n", str_even1);
	printf("str_odd1: %s\n", str_odd1);
	printf("str_even1 reversed: %s\n", strrev(str_even1));
	printf("str_odd1 reversed: %s\n", strrev(str_odd1));
	
	//test strrev2 (which uses pointers)
	char str_even2[7] = "Hello!";
	char str_odd2[6] = "12345";
	printf("\n\nstr_even2: %s\n", str_even2);
	printf("str_odd2: %s\n", str_odd2);
	printf("str_even2 reversed: %s\n", strrev(str_even2));
	printf("str_odd2 reversed: %s\n", strrev(str_odd2));

}

/*
 * strrev()
 *
 * reverses a string using array indexing
 * params: character pointer, pointing to string
 * return: character pointer, pointing to reversed string
 */
char *strrev(char *s)
{
	int i = 0;
	int nti = 0;	//null terminator index
	//find index of null terminator
	while(s[i++])
		nti++;	//will increment one less time than i
	//find rightmost string char
	int right = nti - 1;
	//initialize to leftmost string char
	int left = 0;
	//while left and right haven't met (odd string length) or passed each other (even string length)
	while(left < right)
	{
		//store left at nti (used as temp var here)
		s[nti] = s[left];
		//store right at left
		s[left] = s[right];
		//store nti at right
		s[right] = s[nti];
		left++;		//incr left
		right--;	//decr right
	}
	//replace null terminator
	s[nti] = '\0';
	return s;
}

/*
 * strrev2()
 *
 * reverses a string using pointer arithmetic
 * params: character pointer, pointing to string
 * return: character pointer, pointing to reversed string
 */
char *strrev2(char *s)
{
	int i = 0;
	char *ntp = 0;	//null terminator pointer
	//find offset of null terminator and have ntp point to it
	while(*ntp++);
	//point with our left hand, so to speak
	char *left = s;
	//and with the right
	char *right = ntp - 1;
	//while left and right haven't met yet (odd string length) or passed each other (even string length)
	while(left < right)
	{
		//store left at ntp spot
		*ntp = *left;
		//store right at left
		*left = *right;
		//store ntp at right
		*right = *ntp;
		left++;
		right--;
	}
	//replace null terminator
	*ntp = '\0';
	return s;
}
