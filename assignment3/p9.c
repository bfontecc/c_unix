#include <stdio.h>
#include <string.h>
main() 
{
    char *p;
    printf("The string \"Hello\" has the value %u\n", "Hello");
    p = "Hello";
    printf("The variable p has the value %u\n", p);
    printf("The value of \"Hello\"+1 is %u\n", "Hello"+1);
    p = "Hello" + 1;
    printf("The variable p now has the value %u\n", p);
    printf("The length of p+1 is %d\n", strlen(p+1));	
}
