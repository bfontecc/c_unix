#include <stdio.h>

main()
{
			//n[0]  n[1]    n[2]      n[3]
	char n[4][10] = {"ann", "bob", "carol", "dave"};
	char *p1;
	char *p2;
	p1 = n[2];
	p2 = n[2];

	//ok
	printf("n is %u, n[0] is %u, n[0][0] is %u\n", n, n[0], n[0][0]);	

	//error				//here
	printf("(n+1) is %u, (n+1)[0] is %u, (n+1)[0][0] is %u\n", (n+1), (n+1)[0], (n+1)[0][0]);

	//ok
	printf("p1 is %u, p1[0] is %u\n", p1, p1[0]);

	//error
	//printf("p1[0][0] is %u\n", p1[0][0]);
	printf("p1 addr:%u\n", &p1);

	//ok, same as above ok one
	printf("p2 is %u, p2[0] is %u\n", p2, p2[0]);

	//error
	//printf("p2[0][0] is %u\n", p2[0][0]);
	printf("p2 addr: %u\n", &p2);
}
