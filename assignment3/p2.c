/**
 * p2.c
 *
 * problem 2 of assignment 3
 * need to add code that prints out the value and location of all 8 variables
 */

#include <stdio.h>

char t[3] = "hi";

main()
{
	int x , y[4] = { 71, 181, 30, 131 } ;
	char a[30] = "pointers store addresses.";
	int *q;
	char *p;
	int *s;
	int **r;
	
	p = a + 4;
	q = &y[0];
	s = &x;
	*s = '0';
	x = 44;
	r = &q;
	*q = q[2] + --x;
	
	//write code here to print out values and locations of all 8 vars
	
	printf("t is located at %u, t contains %s\n", t, t);
	printf("x is located at %u, x contains %d\n", &x, x);
	printf("q is located at %u, q contains %d\n", q, *q);
	printf("p is located at %u, p contains %c\n", p, *p);
	printf("s is located at %u, s contains %d\n", s, *s);
	printf("r is located at %u, r contains %u\n", r, *r);
	printf("y is located at %u, y contains:\n", y);
	printf("a is located at %u, a contains%s\n", a, a);
	
	//print int array y
	int i = 0;
	while(i < (sizeof(y) / sizeof(y[0])))
	{
		printf("\t%d\n", y[i]);
		i++;
	} 
}
