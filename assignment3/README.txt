	1.
address numbers
	2.
see problem_2.png
see p2.c
	3.
a.)
m is 3218025116, &m is 3218025116, &m[0] is 3218025116, m[0] is 4
p is 3218025124, &p is 3218025136, &p[0] is 3218025124, p[0] is 2

b.)
see problem_3.png

c.)
They are all the address of the first element in the array. The array name, m, is an address. The address of an address, &m, is just that address. The address of the first array element is just another way of asking for the address of the array. Therefore, for array m: m, &m, and &m[0] are all the same.

d.)
p is a pointer variable, which is stored somewhere in memory. The value stored in p may point to an array, or somewhere in an array, but p is not an array. p's value is that which p points to. p[0] is equivalent to p because it indexes zero items away from that which points to. The address of p, though, is a different story. The address of p is the spot where p is stored, regardless of what it points to or doesn't point to.

e.)
p[-1] should be m[1] in this case. The value of m[1] is 1, so p[-1] is 1. p[-2] points to the array item 2 items back from p. Where p points to m+2 in this case, p[-2] is simply the value at m, or m[0]. It should be 0 in this case.

	4.
a.)
printf("p2[0][0] is %u\n", p2[0][0]);
printf("p1[0][0] is %u\n", p1[0][0]);

also, on my machine, the line:
p2 = n + 2;
yields an assignment to incompatible pointer type error.

b.)
n is 3220711760, n[0] is 3220711760, n[0][0] is 97
(n+1) is 3220711770, (n+1)[0] is 3220711770, (n+1)[0][0] is 98
p1 is 3220711780, p1[0] is 99
p2 is 3220711780, p2[0] is 99
This is the output I got, but I had to change 'p2 = n + 2;' to 'p2 = n[2]'
I believe these two are equivalent in output.

c.)
see problem_4.png

d.)
They both point to a character (which heads an inner array) at a position two array elements (arrays in this case) forward of n (the first element in the array's address).

e.)
p2+1 should be the address of n[2], which would end in 80, in this case.
I can't really check this, due to the above-mentioned error.

	5.
a.)
	a[2] = 4
b.)
	*(a + 2) = 4
c.)
	a[4] - a[2] = 2
d.)
	a[a[4]-2] = a[4] = 6
e.)
	(a + 10)[-8] = a[2] = 4
	
	6.
a.)
	a[0]
b.)
	a[3]
c.)
	a[12] (undefined)
d.)
	a[2]
e.)
	a[-10] (undefined)

	7.
a.)
	x = 6
	a = { 4, 10, 4, 3, 6, 13 }
	p points to: a
b.)
	x = 4 
	a = { 4, 10, 4, 3, 6, 13 }
	p points to: a
c.)
	x = 6
	a = { 4, 10, 4, 3, 6, 13 }
	p points to: a[1]
d.)
	x = 10
	a = { 4, 10, 4, 3, 6, 13 }
	p points to: a[1]
e.)
	x = 5
	a = { 5, 10, 4, 3, 6, 13 }
	p points to: a[0]
f.)
	x = 4
	a = { 4, 10, 4, 3, 6, 13 }
	p points to: a[1]
g.)
	x = 4
	a = { 5, 10, 4, 3, 6, 13 }
	p points to: a[0]
h.)
	x = 6
	a = { 4, 10, 4, 3, 6, 13 }
	p points to: a[0]

	8.
Returns address of local variable.
	This function attempts to return the address of squares. squares will no longer exist when this function returns
Segmentation Fault
	This function's loop goes to i<=n, which indexes array squares with length n to squares[n]. It only goes up to squares[n-1] and then experiences a problem.
Buffer overflow 
	When n^2 > the max int val for this compiler, the boundary in the array will be overrun.

	9.
a.)
The string "Hello" has the value 134513920
The variable p has the value 134513920
The value of "Hello"+1 is 134513921
The variable p now has the value 134513921
The length of p+1 is 3
b.)
see problem_9.png
c.)
The compiler is  using one stored instance of the string literal "Hello". Whenever "Hello" is used, it is like using the address of this area in memory. When p points to it, p is pointing to all of the string. When p points one forward, it is one character (one byte) forward. The expression (p+1) where p = "Hello" + 1, is equivalent to "Hello" + 2. This points to the 'l' character. The two l's and the o have a count of three, and the null terminator doesn't count toward length.

	10.
a.)
'd'
b.)
Yes, it's legal. This expression tells the compiler to make a string literal, then index the 3rd item in the string literal. In this case the 3rd item (always a character) is the character 'd'.

	11.
see p11.c
	12.
see problem_12.png
	13.
struct stop a[10];
defines an array of 12 stop structs. The array will be like any other, except each item is a struct.

struct stop* b[10];
defines a (smaller) array of 12 pointers, which are of type struct stop. They don't point to any particular structs. These (or at least it) would have to be created before the pointers can be assigned their addresses.

see problem_13.png
